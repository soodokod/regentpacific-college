<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () { return view('welcome'); });

Route::get('/', function () { return view('home'); })->name('home');

Route::get('/about', function () { return view('about'); })->name('about');

Route::get('/curriculum', function () { return view('curriculum'); })->name('curriculum');

Route::get('/tuition', function () { return view('tuition'); })->name('tuition');

Route::get('/contact', function () { return view('contact'); })->name('contact');