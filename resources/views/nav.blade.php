<nav class="navbar navbar-expand-lg navbar navbar-dark">
	<a class="navbar-brand" href="javascript:void(0)">
		<img src="/library/img/favicon.ico" width="30" height="30" class="d-inline-block align-top" alt="">
		{{ config('app.name') }}
	</a>
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="navbarNav">
		<ul class="navbar-nav">
			<li class="nav-item @if (Request::is('/')) active @endif">
				<a class="nav-link" href="{{ route('home') }}">
					Home
					@if (Request::is('/')) <span class="sr-only">(current)</span> @endif
				</a>
			</li>
			<li class="nav-item @if (Request::is('about')) active @endif">
				<a class="nav-link" href="{{ route('about') }}">
					About us
					@if (Request::is('about')) <span class="sr-only">(current)</span> @endif
				</a>
			</li>
			<li class="nav-item @if (Request::is('curriculum')) active @endif">
				<a class="nav-link" href="{{ route('curriculum') }}">
					Curriculum
					@if (Request::is('curriculum')) <span class="sr-only">(current)</span> @endif
				</a>
			</li>
			<li class="nav-item @if (Request::is('tuition')) active @endif">
				<a class="nav-link" href="{{ route('tuition') }}">
					Tuition & Accomodation
					@if (Request::is('tuition')) <span class="sr-only">(current)</span> @endif
				</a>
			</li>
			<li class="nav-item @if (Request::is('contact')) active @endif">
				<a class="nav-link" href="{{ route('contact') }}">
					Contact us
					@if (Request::is('contact')) <span class="sr-only">(current)</span> @endif
				</a>
			</li>
		</ul>
	</div>
</nav>