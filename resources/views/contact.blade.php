@extends('layouts.app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-6 order-md-2" style="padding-left: 0px!important; padding-right: 0px!important;">
			<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" style="padding-top: 0px!important;">
				<ol class="carousel-indicators">
					<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
					<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
					<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
				</ol>
				<div class="carousel-inner">
					<div class="carousel-item active">
						<img class="d-block w-100" src="/library/img/rpc_img27.jpg" alt="...">
					</div>
					<div class="carousel-item">
						<img class="d-block w-100" src="/library/img/rpc_img23.jpg" alt="...">
					</div>
					<div class="carousel-item">
						<img class="d-block w-100" src="/library/img/rpc_img21.jpg" alt="...">
					</div>
				</div>
			</div>
			<div class="contact-details bg-light p-3">
				<h5>Brgy. Agus, Lapu-Lapu City, 6015 Cebu Philippines</h5>
				<div class="row">
					<div class="col-md-6">
						<label>Telephone</label>
						<h3>(032) 495 - 2326</h3>
					</div>
					<div class="col-md-6">
						<label>KakaoTalk</label>
						<h3>(+63) 921 0357 489</h3>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<label>Email</label>
						<h3>agus_rpc@yahoo.com</h3>
					</div>
					<div class="col-md-6">
						<label>Line</label>
						<h3>rpc_cebuph</h3>
					</div>
				</div>
			</div>
		</div>
		<div id="map" class="fh-nh col-md-6 order-md-1 bg-light"></div>
	</div>
</div>

<script type="text/javascript">
	var map;

	function initMap() {
		var rpc = new google.maps.LatLng(10.278280, 123.986669);

	    map = new google.maps.Map(document.getElementById('map'), {
	        center: rpc,
	        zoom: 19,
	        mapTypeId: 'hybrid'
	    });

	    // Config Icon
        var icon = {
			url: 'https://img.icons8.com/office/80/000000/marker.png',
			scaledSize: new google.maps.Size(70, 70)
		};

		new google.maps.Marker({
			position: rpc,
			icon: icon,
			map: map
		});
	}
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC-TZ86VboRzEgTZMTG82h84j4IrOoK4rE&callback=initMap" async defer></script>
@endsection