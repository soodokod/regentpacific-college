@extends('layouts.app')

@section('content')
<!-- class="d-flex flex-column flex-md-row align-items-md-center p-5 bg-light" -->
<header class="container-fluid bg-light">
	<div class="row align-items-center p-5">
		<div class="pt-md-3 pb-md-4 col-md-7">
			<h1 class="bd-title mt-0">Highly Qualified ESL Education</h1>
			<!-- <h1 class="bd-title mt-0">Well-Trained ESL Education</h1> -->
			<!-- <h1 class="bd-title mt-0">Fun and Effective Personalized Service</h1> -->
			<p class="bd-lead">It is a pre-eminent learning institution in the Philippines of mixed races, aims to provide world class education to the clientele characterized as a fully-functioning person through academic excellence and values formation.</p>
			<button type="button" class="btn btn-outline-primary">Get Information</button>
		</div>
		<div class="col-md-5">
			<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
				<ol class="carousel-indicators">
					<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
					<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
					<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
				</ol>
				<div class="carousel-inner">
					<div class="carousel-item active">
						<img class="d-block w-100" src="/library/img/rpc_img1.jpg" alt="...">
					</div>
					<div class="carousel-item">
						<img class="d-block w-100" src="/library/img/rpc_img4.jpg" alt="...">
					</div>
					<div class="carousel-item">
						<img class="d-block w-100" src="/library/img/rpc_img5.jpg" alt="...">
					</div>
				</div>
			</div>
		</div>
	</div>
</header>

<div class="container-fluid">
	<div class="events-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
		<h1 class="bd-title mt-0">Events & Activities</h1>
		<p class="bd-lead">This is the perfect place for you to learn English fast, and meet international friends as you relax in a fun and safe environment.</p>
	</div>	

	<div class="row pl-md-5 pr-md-5">
		<div class="col-md-6">
			<div class="card flex-md-row mb-4 mb-md-5 h-95">
				<div class="card-body d-flex flex-column">
					<strong class="d-inline-block mb-2 text-success">Seminars and Conferences</strong>
					<h3 class="card-title mb-1">TESOL Seminar Certificate</h3>
					<p class="card-text m-0"><small class="text-muted">Nov 11, 2017</small></p>
					<p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content.</p>
					<a href="javascript:void(0)">Continue reading</a>
					<p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
				</div>
				<img class="image-right flex-auto d-none d-lg-block" src="/library/img/rpc_img21.jpg" data-holder-rendered="true" alt="...">
			</div>
		</div>
		<div class="col-md-6">
			<div class="card flex-md-row mb-4 mb-md-5 h-95">
				<div class="card-body d-flex flex-column">
					<strong class="d-inline-block mb-2 text-success">Seminars and Conferences</strong>
					<h3 class="card-title mb-1">TESOL Seminar Certificate</h3>
					<p class="card-text m-0"><small class="text-muted">Nov 11, 2017</small></p>
					<p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content.</p>
					<a href="javascript:void(0)">Continue reading</a>
					<p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
				</div>
				<img class="image-right flex-auto d-none d-lg-block" src="/library/img/rpc_img23.jpg" data-holder-rendered="true" alt="...">
			</div>
		</div>
		<div class="col-md-6">
			<div class="card flex-md-row mb-4 mb-md-5 h-95">
				<div class="card-body d-flex flex-column">
					<strong class="d-inline-block mb-2 text-success">Seminars and Conferences</strong>
					<h3 class="card-title mb-1">TESOL Seminar Certificate</h3>
					<p class="card-text m-0"><small class="text-muted">Nov 11, 2017</small></p>
					<p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content.</p>
					<a href="javascript:void(0)">Continue reading</a>
					<p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
				</div>
				<img class="image-right flex-auto d-none d-lg-block" src="/library/img/rpc_img24.jpg" data-holder-rendered="true" alt="...">
			</div>
		</div>
		<div class="col-md-6">
			<div class="card flex-md-row mb-4 mb-md-5 h-95">
				<div class="card-body d-flex flex-column">
					<strong class="d-inline-block mb-2 text-success">Seminars and Conferences</strong>
					<h3 class="card-title mb-1">TESOL Seminar Certificate</h3>
					<p class="card-text m-0"><small class="text-muted">Nov 11, 2017</small></p>
					<p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content.</p>
					<a href="javascript:void(0)">Continue reading</a>
					<p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
				</div>
				<img class="image-right flex-auto d-none d-lg-block" src="/library/img/rpc_img25.jpg" data-holder-rendered="true" alt="...">
			</div>
		</div>
	</div>
</div>
@endsection