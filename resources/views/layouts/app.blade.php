<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<link rel="shortcut icon" type="text/css" href="{{ asset('library/img/favicon.ico') }}" type="image/x-icon">

		<link rel="stylesheet" type="text/css" href="{{ asset('library/bootstrap/4.1.3/css/bootstrap.min.css') }}">

		<title>{{ config('app.name') }}</title>

		<link rel="stylesheet" type="text/css" href="{{ asset('library/app.css') }}">
	</head>
	<body>
		
		@include('nav')

		@yield('content')
		
		<script type="text/javascript" src="{{ asset('library/jquery/3.3.1/jquery.slim.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('library/popper/1.14.3/popper.min.js') }}"></script>
		<script type="text/javascript" src="{{ asset('library/bootstrap/4.1.3/js/bootstrap.min.js') }}"></script>
	</body>
</html>