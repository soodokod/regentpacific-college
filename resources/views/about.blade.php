@extends('layouts.app')

@section('content')
<div class="container">
	<div class="bd-masthead">
		<div class="row align-items-center">
			<div class="mx-auto col-md-6 order-md-2">
				<img class="img-fluid mb-3 mb-md-0" src="/library/img/rpc_img27.jpg" alt="" width="1024" height="860">
			</div>
			<div class="col-md-6 order-md-1 text-center text-md-left pr-md-5">
				<h1 class="mb-3 bd-text-purple-bright">Campus Environment</h1>
				<p class="lead">The vast site overlooking the sea of emerald green and blooming tropical people with emotions full of colorful flowers is to heal the weary head preeminent environment.</p>
				<p class="lead mb-4">On campus, such as a large swimming pool and various sports facilities, you can refresh and equipped with full facilities. Aside from staying for learning English, RPC campus environment can provide a relaxation for both physical and mental stability.</p>
			</div>
		</div>
	</div>
	<div class="bd-masthead">
		<div class="row align-items-center">
			<div class="mx-auto col-md-6 order-md-1">
				<img class="img-fluid mb-3 mb-md-0" src="/library/img/rpc_img4.jpg" alt="" width="1024" height="860">
			</div>
			<div class="col-md-6 order-md-1 text-center text-md-left pl-md-5">
				<h1 class="mb-3 bd-text-purple-bright">Friendly and Qualified Teachers</h1>
				<p class="lead">RPC ESL Center has lots of friendly and qualified instructors who are staying in the campus dormitory. With this, it can also promote a better communication for the students. To learn a new language effectively, it is important to practice every learning in actual application.</p>
				<p class="lead mb-4">Through practicing English communication in the 24-hour English pickled environment, it is possible to learn English naturally. Cross-cultural exchange, activities, sports and various scenes, can promote and developed through applying onto its English environment.</p>
			</div>
		</div>
	</div>

	<div class="bd-masthead">
		<div class="row align-items-center">
			<div class="mx-auto col-md-6 order-md-2">
				<img class="img-fluid mb-3 mb-md-0" src="/library/img/rpc_img21.jpg" alt="" width="1024" height="860">
			</div>
			<div class="col-md-6 order-md-1 text-center text-md-left pr-md-5">
				<h1 class="mb-3 bd-text-purple-bright">Basic Elementary Education and High School</h1>
				<p class="lead">The RPC Campus has a hostel and international school that caters the needs of Basic Elementary Education and High School. It is also an environment that can be fun and interact with local students.</p>
				<p class="lead mb-4">There is also a joint event with the students of the International School; students can learn a valuable experience through international exchange. If high school students or less, it is also possible to participate in the class of the International School (surcharge). In addition, RPC safeguards a 24 hour on duty security guards that provides the safety of every individuals coming from the campus.</p>
			</div>
		</div>
	</div>
</div>
@endsection